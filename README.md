## About (in English)
This is a fork from [linux-dash](https://github.com/afaqurk/linux-dash) hosted on github.
More information on the original project can be found on [the dedicated website www.LinuxDash.com](http://www.linuxdash.com/).
### WARNING: my fork is **not** up-to-date !

The goal of this fork is to provide a **French translation** to the user interface of *linux-dash*.
My version is **not always up-to-date** (even if I try!).

[**View DEMO**](http://www.linuxdash.com/) | [**View Features**](#features) | [**Installation Instructions**](#installation) | [**News**](https://github.com/afaqurk/linux-dash#news) | [**Documentation**](https://github.com/afaqurk/linux-dash/wiki)

## À-propos (en français)
Ceci est une bifurcation depuis [linux-dash](https://github.com/afaqurk/linux-dash) hébergé sur github.
Plus d'informations sur le projet original peuvent être trouvées sur [le site dédié www.LinuxDash.com](http://www.linuxdash.com/).

Le but de cette bifurcation est de proposer une traduction francophone de l'interface utilisateur de *linux-dash*.
Ma version n'est **pas à jour** !

---

# linux-dash
Un panneau de contrôle système *très léger* pour une machine sous GNU/Linux.
Il suffit de [télécharger l'application](https://bitbucket.org/lbesson/linux-dash/get/master.zip), et c'est tout !

[Voir une démo ?](http://www.linuxdash.com/demo.html) | [**Fonctionnalités**](#fonctionnalités) | [**Comment l'installer**](#installation)

![Démonstration](https://bytebucket.org/lbesson/linux-dash/raw/master/linux-dash.png "Démonstration")

## Fonctionnalités
* Un joli panneau de contrôle en ligne pour contrôler l'état d'un serveur,
* Surveillance à-la-demande pour la mémoire RAM, la charge système, le temps de démarrage, l'espace disque, les utilisateurs connectés, et plein d'autres variables !
* Installation en un clic sur les serveurs Apache2/nginx + PHP,
* Cliquer et glisser pour ré-arranger les différents composants,
* Supporte de nombreux types de serveurs Linux différents [(voir la section Support)](#support)

## Installation
1. Vérifiez que vous avez bien `php5-curl` installé et accessible
2. Téléchargez l'archive zip, ou clonez le dépôt git
3. Placez le quelque part dans un dossier accessible par le serveur web (Apache ou Nginx ou autre). Par exemple `/var/www/` pour Apache
4. (*optionnel*) Sécurisez l'accès à ces pages via un `.htaccess` (ou une autre méthode de votre choix). Un fichier `.htaccess` par défaut est inclus, **mais il faut penser à changer le chemin vers le fichier contenant les mots de passes** (`.htpasswd`)

> **Attention: Pour limiter le risque d'attaque ou de fuite d'information,**
> **il est vraiment recommandé de restreindre l'accès à ce dossier.**

## Support
*Les informations listées ici sont actuellement limitée et vont potentiellement évoluer.*

* Systèmes d'exploitation (Linux)
    * Arch
    * Debian 6, 7
    * Ubuntu 11.04+
    * Linux Mint 16+
    * CentOS 5, 6

* Apache 2+,
* Nginx 1+,
* PHP 5+,
* Navigateurs modernes.

## Merci à...
* [Dashboard Template](http://www.egrappler.com/templatevamp-free-twitter-bootstrap-admin-template/)
* [Bootstrap](http://getbootstrap.com)
* [Font Awesome](http://fontawesome.io/)

---

### Traduit (et bidouillé) par [Lilian Besson](<https://bitbucket.org/lbesson>).

### Langages
 - PHP
 - JavaScript,
 - HTML 5,
 - CSS 3.

### Licence
Ce projet est distribué publiquement sous les conditions de la **licence GPLv3**.
Pour plus de détails, veuillez lire le fichier [LICENSE](http://besson.qc.to/LICENSE.html) inclus dans les sources.

*En gros, ça vous autorise à utiliser n'importe quel morceau de ce projet pour vos propres projets.*
